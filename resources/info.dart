
class Info{
  static const String client_id = "8cvnmMyUR1MhCSxNQu3jYXaK0Sga";
  static const String client_secret = "B4pjhmVW6SAr6tJ5rFpr1nGJdq4a";

  static const String redirect_uri = "sumra.infinity://oauth";

  static const String authorization_scope = "openid";
  static const String authorization_endpoint = "https://api.sumra.net/oauth2/authorize";
  static const String end_session_endpoint = "https://api.sumra.net/oidc/logout";

  static const String token_endpoint = "https://api.sumra.net/token";
  static const String revoke_endpoint = "https://api.sumra.net/revoke";

  static const String userinfo_endpoint = "https://api.sumra.net/oauth2/userinfo";
  static const bool https_required = true;
}