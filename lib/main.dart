import 'package:flutter/material.dart';
import 'package:infinity_card/utils/AppColors.dart';
import 'package:infinity_card/utils/GoogleOAuth2Client.dart';
import 'package:infinity_card/utils/info_text.dart';
import 'package:infinity_card/widgets/WidgetCards.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:infinity_card/widgets/WidgetProfile.dart';
import 'package:infinity_card/widgets/WidgetSplash.dart';
import 'package:infinity_card/widgets/WidgetTariff.dart';

import 'package:openid_client/openid_client.dart' as R ;
// import 'package:flutter/src/widgets/basic.dart';
// import 'package:openid_client/src/openid.dart';

import 'dart:io';
import 'package:oauth2/oauth2.dart' as oauth2;

// import "package:oauth2_client/oauth2_client.dart";
import 'package:meta/meta.dart';

import 'dart:developer' as developer;

final authorizationEndpoint =
    Uri.parse('https://am.sumra.net/oauth2/authorize');
final tokenEndpoint = Uri.parse('https://am.sumra.net/oauth2/token');
final redirectUrl = Uri.parse('wso2iswallet://oauth');

final identifier = 'Bgmec7B7oG0aTUrAbRCDBpc_xXEa';
final secret = '0TPCiRwwBvgYxbMufGjnBpuS5Oka';
final credentialsFile = File('~/.myapp/credentials.json');

// Future<oauth2.Client> createClient() async {
//   var exists = await credentialsFile.exists();
// // Якщо облікові дані OAuth2 вже були збережені з попереднього запуску, ми
// // просто хочемо їх перезавантажити.
//   if (exists) {
//     var credentials = oauth2.Credentials.fromJson(await credentialsFile.readAsString());
//     return oauth2.Client(credentials, identifier: identifier, secret: secret);
//   }
// // Якщо у нас ще немає облікових даних OAuth2, нам потрібно отримати власника ресурсу
// // уповноважити нас. Тут ми припускаємо, що ми додаток командного рядка.
//   var grant = oauth2.AuthorizationCodeGrant(
//       identifier, authorizationEndpoint, tokenEndpoint, secret: secret);
// // URL-адреса на сервері авторизації (авторизаціяEndpoint з деякими додатковими
// // параметри запиту). Області застосування та стан можуть бути необов’язково передані в цей метод.
//   var authorizationUrl = grant.getAuthorizationUrl(redirectUrl);
// // Перенаправити власника ресурсу на URL-адресу авторизації. Одного разу ресурс
// // власник авторизував, вони будуть перенаправлені на `redirectUrl` за допомогою
// // код авторизації. `Перенаправлення` має призвести до переспрямування браузера на
// // інша URL-адреса, яка також повинна мати прослуховувач.
// // тут не показано перенаправлення та прослуховування. Див. Нижче для деталі.
// //   await redirect(authorizationUrl);
// //   var responseUrl = await listen(redirectUrl);
//
// // Після того, як користувач буде перенаправлений на `redirectUrl`, передайте параметри запиту в
// // AuthorizationCodeGrant. Він перевірить їх і витягне код авторизації для створення нового Клієнта.
//   return await grant.handleAuthorizationResponse(responseUrl.queryParameters);
// }

var screenHome = "/";
var screenTariffs = "/tariffs";
var screenProfile = "/profile";

FlutterAppAuth appAuth = FlutterAppAuth();

void main() async {

  runApp(WidgetSplash());
  print("Start process oAuth2 init");

  var data = new SumraOAuth2Client(
      redirectUri: info_text.redirect_uri, customUriScheme: info_text.uri_shem);

  print("Request oAuth2....");
  var resp;
  try {
    resp = await data.getTokenWithAuthCodeFlow(
        clientId: info_text.client_id,
        scopes: [
          info_text.authorization_scope
        ]); //, 'profile', 'email', 'offline_access', 'api']);
    print("Operation success -> ${resp}");
  }catch(Error){
    print("Message error -> ${resp.error}");
  }finally{
    print("Message error (finally block)");
  }

}

/*
curl -k -X POST https://api.sumra.net/token -d "grant_type=password&username=p.s.g.sirogk&password=5BUdeKe8sn9FWYgL" -H "Authorization: Basic OGN2bm1NeVVSMU1oQ1N4TlF1M2pZWGFLMFNnYTpCNHBqaG1WVzZTQXI2dEo1ckZwcjFuR0pkcTRh"
resp->
{"access_token":"eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJwLnMuZy5zaXJvZ2siLCJhdWQiOiI4Y3ZubU15VVIxTWhDU3hOUXUzallYYUswU2dhIiwibmJmIjoxNjAzNDY2MDM4LCJhenAiOiI4Y3ZubU15VVIxTWhDU3hOUXUzallYYUswU2dhIiwiaXNzIjoiaHR0cHM6XC9cL2FnLnN1bXJhLm5ldFwvb2F1dGgyXC90b2tlbiIsImV4cCI6MTYwMzQ2OTYzOCwiaWF0IjoxNjAzNDY2MDM4LCJqdGkiOiIyZWUxMTViNy05ZWE5LTQ0ZDMtYTFjYy1jOWYyNzY4OTZkYWMifQ.luUzuzKvP84yiLhHobYAKUkyK4RePHjcEPt7nRMzezHXAktqtaSFOPLhhZYBlRTQSa2EzX8qLZj0W4Mj16NhYeEPO3RtfoeMKmdmjCufkShyBT_W9psKExLIVZDcJMYGAofcQlWt7iqti9jiacqqSsJlHLnDi5OsZ2BO_qbl2S714QUQoUYSKZQcQCbFpmz_M9CkG8ZZAj5GoYOQKV9Sn7NTHlJE9XL1F55qkCXxBwjNklRNebQYaihlYC0837zBYDEZBLO4MdAO1eNkh5pOQZ30tnMPjLK92ATHHAT_Sdx1-dsyyJULYcFk7KlbLrKr9H-BC6P0qoCzcan3TwdYig",
"refresh_token":"173bc7ee-1f1f-32aa-9ca0-701b65490cd8",
"token_type":"Bearer",
"expires_in":3600}
===============================
curl -k -X POST https://apim.sumra.net/oauth2/token -d "grant_type=password&username=p.s.g.sirogk&password=5BUdeKe8sn9FWYgL" -H "Authorization: Basic OGN2bm1NeVVSMU1oQ1N4TlF1M2pZWGFLMFNnYTpCNHBqaG1WVzZTQXI2dEo1ckZwcjFuR0pkcTRh"
resp->
{"access_token":"eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJwLnMuZy5zaXJvZ2siLCJhdWQiOiI4Y3ZubU15VVIxTWhDU3hOUXUzallYYUswU2dhIiwibmJmIjoxNjAzNDY2NzY2LCJhenAiOiI4Y3ZubU15VVIxTWhDU3hOUXUzallYYUswU2dhIiwic2NvcGUiOiJkZWZhdWx0IiwiaXNzIjoiaHR0cHM6XC9cL2FwaS5zdW1yYS5uZXRcL29hdXRoMlwvdG9rZW4iLCJleHAiOjE2MDM0NzAzNjYsImlhdCI6MTYwMzQ2Njc2NiwianRpIjoiZTg0ZTE4NWUtMWUxNi00NzQ5LWEwZWMtMGYwNjJmNTM2MjY5In0.v74NT9qjbHaUDNNJyX8eM6SY7NnMaxHlAB7T-aSY6btQ5o6WOjdddFFGTNQT4KPlMTzZ95v7_NHubdR-xNCKKzKkw_U4rsqKUB19-HDHhRk2iRGJRrbtNP59I9NvA0NH6sCJQhOgF3qRQ99cziKKR-4aV4UTcpnc5ugoZrqjR1Fugh6jiOBqV71PfmCOi0Pu_j8rB94BdzwWk3nhq9cK4yQKmTRI-I1HLD57KvMSw9jkvsx9Rx6cFlO33QULYOG54XgasdE-4u88qU-TU1aI6g_mmGaDWyrl0-kqBm9b_Fs6v8hNi0WSDq0K15JlNmrYoy4WaKeSV1urBpiss8rYxQ",
"refresh_token":"4c1980b8-6c0d-3c0e-9d9b-8ca37aeddb36",
"scope":"default",
"token_type":"Bearer",
"expires_in":3600}
 */