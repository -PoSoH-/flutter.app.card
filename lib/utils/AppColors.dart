// Sumra infinity card app colors file

import 'dart:ui' show Color;
import 'package:flutter/painting.dart';

class AppColors{
    // extends ColorSwatch<int> {AppColors(int primary, Map<int, Color> swatch) : super(primary, swatch);

  static const Color accent_light = Color(0xFFFF872D);
  static const Color accent_dark  = Color(0xFFC43C00);
  // static const int accent_base  = 0xFFFF6D00;

  static const Color gray_normal = Color(0xFF777777);
  static const Color gray_light  = Color(0xFFBBBBBB);
  static const Color gray_dark   = Color(0xFF444444);

  static const Color text_color_l   = Color(0xFF000000);
  static const Color text_color_d   = Color(0xFFFFFFFF);

  static const Color text_to_color   = Color(0xFF0A9629);
  static const Color text_from_color   = Color(0xFFF31E1E);

  static const Color color_progress_l = Color(0xAA000000);
  static const Color color_progress_d = Color(0xAAFFFFFF);

  static const Color color_press_no = Color(0xFFFF6D00);
  static const Color color_press_ok = Color(0xFFC43C00);

  static const Color color_card_back_l = Color(0xFFFBF4E1);
  static const Color color_card_back_d = Color(0xFF444444);

  static const Color color_background_start = Color(0xFF000000);
  static const Color color_background_final  = Color(0xFF373737);

  static const Color color_gold_start = Color(0xFFCA8F31);
  static const Color color_gold_final = Color(0xFFFEF89E);

}

// Color(0xFFCA8F31), Color(0xFFFEF89E)