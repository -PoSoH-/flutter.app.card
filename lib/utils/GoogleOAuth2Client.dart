import 'package:flutter/material.dart';
import 'package:infinity_card/utils/info_text.dart';
import 'package:oauth2_client/oauth2_client.dart';
import 'package:oauth2_client/oauth2_helper.dart';
//We are going to use the google client for this example...
import 'package:oauth2_client/google_oauth2_client.dart';
import 'package:http/http.dart' as http;

class SumraOAuth2Client extends OAuth2Client {
  //
  // final authorizationEndpoint = Uri.parse('https://am.sumra.net/oauth2/authorize');
  // final tokenEndpoint = Uri.parse('https://am.sumra.net/oauth2/token');
  // final redirectUrl = Uri.parse('wso2iswallet://oauth');
  //
  // final identifier = 'Bgmec7B7oG0aTUrAbRCDBpc_xXEa';
  // final secret = '0TPCiRwwBvgYxbMufGjnBpuS5Oka';
  // final credentialsFile = File('~/.myapp/credentials.json');

  SumraOAuth2Client({@required String redirectUri, @required String customUriScheme}): super(
      authorizeUrl: info_text.authorization_endpoint, // 'https://am.sumra.net/oauth2/authorize', //Your service's authorization url
      tokenUrl: info_text.token_endpoint, //'https://am.sumra.net/oauth2/token', //'https://...', //Your service access token url
      redirectUri: redirectUri,
      customUriScheme: customUriScheme
  );
}

