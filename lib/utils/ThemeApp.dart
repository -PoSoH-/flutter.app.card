

import 'package:flutter/material.dart';

class ThemeApp {
  static ThemeData loadLightTheme(){
    return ThemeData (scaffoldBackgroundColor: Colors.teal,
      appBarTheme: AppBarTheme(
        color: Colors.teal,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      colorScheme: ColorScheme.light(
        primary: Colors.white,
        onPrimary: Colors.white,
        primaryVariant: Colors.white38,
        secondary: Colors.red,
      ),
      cardTheme: CardTheme(
        color: Colors.teal,
      ),
      iconTheme: IconThemeData(
        color: Colors.white54,
      ),
      textTheme: TextTheme(
        title: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
        ),
        subtitle: TextStyle(
          color: Colors.white70,
          fontSize: 18.0,
        ),
      ),
    );
  }

  static ThemeData loadDarkTheme(){
    return ThemeData(
      scaffoldBackgroundColor: Colors.black,
      appBarTheme: AppBarTheme(
        color: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      colorScheme: ColorScheme.light(
        primary: Colors.black,
        onPrimary: Colors.black,
        primaryVariant: Colors.black,
        secondary: Colors.red,
      ),
      cardTheme: CardTheme(
        color: Colors.black,
      ),
      iconTheme: IconThemeData(
        color: Colors.white54,
      ),
      textTheme: TextTheme(
        title: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
        ),
        subtitle: TextStyle(
          color: Colors.white70,
          fontSize: 18.0,
        ),
      ),
    );
  }

  static ThemeData loadDarkGradientTheme(){
    return ThemeData(
      scaffoldBackgroundColor: Colors.black,
      appBarTheme: AppBarTheme(
        color: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      colorScheme: ColorScheme.light(
        primary: Colors.black,
        onPrimary: Colors.black,
        primaryVariant: Colors.black,
        secondary: Colors.red,
      ),
      cardTheme: CardTheme(
        color: Colors.black,
      ),
      iconTheme: IconThemeData(
        color: Colors.white54,
      ),
      textTheme: TextTheme(
        title: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
        ),
        subtitle: TextStyle(
          color: Colors.white70,
          fontSize: 18.0,
        ),
      ),
    );
  }

}