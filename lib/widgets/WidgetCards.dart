import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinity_card/utils/AppColors.dart';
import 'package:infinity_card/utils/AppTextHelp.dart';
import 'package:infinity_card/utils/ThemeApp.dart';
import 'package:infinity_card/widgets/WidgetProfile.dart';
import 'package:infinity_card/widgets/WidgetTariff.dart';
import 'package:gradient_text/gradient_text.dart';

class WidgetCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeApp.loadDarkTheme(),
      home: _WidgetHomeCard(title: 'Cards'),
    );
  }
}

class _WidgetHomeCard extends StatefulWidget {
  _WidgetHomeCard({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeCardPageState createState() => _HomeCardPageState();
}

class _HomeCardPageState extends State<_WidgetHomeCard> {
  int _counter = 0;

  // var _linearTextGradient = LinearGradient(
  //     colors: <Color>[Color(0xFFCA8F31), Color(0xFFFEF89E)]
  // ).createShader(rect: Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  final Shader _linearTextGradient = LinearGradient(
    colors: <Color>[Color(0xFFCA8F31), Color(0xFFFEF89E)],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  var fontOdbSans = GoogleFonts.odibeeSans();

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  int _selectedIndex = 1;
  String _balanceUSD = '${AppTextHelp.symUSD} 110`000';
  String _balanceEUR = '${AppTextHelp.symEUR} 102`685';
  String _balanceGBP = '${AppTextHelp.symGBP} 95`580';

  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Tariffs',
      style: optionStyle,
    ),
    Text(
      'Index 1: Cards',
      style: optionStyle,
    ),
    Text(
      'Index 2: Profile',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch (_selectedIndex) {
        case 0:
          {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => WidgetTariff()),
            );
            break;
          }
        case 2:
          {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => WidgetProfile()),
            );
            break;
          }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(widget.title),
        ),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(17.0, 10.0, 17.0, 10.0),
                  child: Text(
                    'Balance',
                    style: TextStyle(
                        fontFamily: 'sumraNorms',
                        fontSize: 12,
                        color: Color(0x48FFFFFF)),
              ))),
          Padding(
            padding: EdgeInsets.fromLTRB(17.0, 0.0, 17.0, 0.0),
            child:Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: GradientText(
                          _balanceUSD,
                          gradient: LinearGradient(
                              colors: [Color(0xFFCA8F31), Color(0xFFFEF89E)]),
                          style: TextStyle(
                            fontFamily: 'sumraNorms',
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            foreground: Paint()..shader = _linearTextGradient,
                          ),
                        ))),
                    Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: GradientText(
                            _balanceEUR,
                            gradient: LinearGradient(
                                colors: [Color(0xFFCA8F31), Color(0xFFFEF89E)]),
                            style: TextStyle(
                              fontFamily: 'sumraNorms',
                              fontSize: 20,
                              fontWeight: FontWeight.w800,
                            ),
                          ))),
                    Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: GradientText(
                            _balanceGBP,
                            gradient: LinearGradient(
                                colors: [Color(0xFFCA8F31), Color(0xFFFEF89E)]),
                            style: TextStyle(
                              fontFamily: 'sumraNorms',
                              fontSize: 20,
                              fontWeight: FontWeight.w800,
                            ),
                          ))),
              ])),
          Text(
            '$_counter',
            style: Theme.of(context).textTheme.headline4,
          ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
          child: Container(
            width: 56,
            height: 56,
            child: Icon(
              Icons.add,
              color: Colors.white,
              size: 24,
            ),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(colors: [
                  AppColors.color_gold_start,
                  AppColors.color_gold_final
                ])),
          ),
          onPressed: _incrementCounter,
          tooltip:
              'Increment'), // This trailing comma makes auto-formatting nicer for build methods.
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.monetization_on), label: 'Tariffs'),
          BottomNavigationBarItem(
              icon: Icon(Icons.card_giftcard_sharp), label: 'Cards'),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_rounded), label: 'Profile')
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
