import 'package:flutter/material.dart';
import 'package:infinity_card/utils/AppColors.dart';
import 'package:infinity_card/utils/ThemeApp.dart';
import 'package:infinity_card/widgets/WidgetCards.dart';

class WidgetSplash extends StatefulWidget{ //StatelessWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashState();
  }
}

class _SplashState extends State<WidgetSplash>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeApp.loadDarkGradientTheme(),
      home: Center(
           child: Container(
             decoration: BoxDecoration(
               gradient: LinearGradient(
                 begin: Alignment.topCenter,
                 end: Alignment.bottomCenter,
                 colors: [AppColors.color_background_start,AppColors.color_background_final]
               )

             ),
             child: Center(
               // child: Image.asset('icons/ic_infinity_logo.png', width: 400, height: 400,)
                 child: MaterialApp(
                     home: Builder(
                         builder: (context) => new GestureDetector(
                             onTap: (){
                               print('LOGO PRESSED');
                               _loadNextPage(context);
                             },
                             child: Image(
                               image: AssetImage('resources/icons/ic_infinity_logo.png'),
                             )
                         )))
               // child: Text('infinity card'),
             ),
           ),
          ),
    );
  }

  void _loadNextPage(BuildContext context){
    setState(() {
      print(" ... tapped ... ");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => WidgetCards()),
      );
    });
  }

}